# Require any additional compass plugins here.
#add_import_path "assets/includes/foundation-sites/assets/"
add_import_path "assets/includes/foundation-sites/scss/"

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = ""
sass_dir = "assets/scss"
images_dir = "assets/images"
font_url="assets/fonts"
javascripts_dir = "assets/js"