<?php get_header(); ?>

    <section class="main" role="main">

        <h1><?php the_title(); ?></h1>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <?php the_content(); ?>

                <br class="clear">

            </article>

        <?php endwhile; endif; ?>

    </section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
