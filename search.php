<?php get_header(); ?>

    <section class="main" role="main">

        <h1><?php echo sprintf( __( '%s Search Results for ', 'paperplane' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>

        <?php get_template_part('loop', 'search'); ?>

        <?php get_template_part('pagination'); ?>

    </section>


<?php get_sidebar(); ?>

<?php get_footer(); ?>
