<?php get_header(); ?>

    <section class="main" role="main">

    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                </a>
            <?php endif; ?>

            <h1><?php the_title(); ?></h1>

            <span class="date"><?php the_date(); ?> <?php the_time(); ?></span>
            <span class="author"><?php _e( 'Published by', 'paperplane' ); ?> <?php the_author_posts_link(); ?></span>
            <span class="comments"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'paperplane' ), __( '1 Comment', 'paperplane' ), __( '% Comments', 'paperplane' )); ?></span>

            <?php the_content(); // Dynamic Content ?>

            <?php the_tags( __( 'Tags: ', 'paperplane' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>

            <p><?php _e( 'Categorised in: ', 'paperplane' ); the_category(', '); // Separated by commas ?></p>

            <p><?php _e( 'This post was written by ', 'paperplane' ); the_author(); ?></p>

            <p><?php echo paperplane_postsharer();  ?></p>

            <?php comments_template(); ?>

        </article>

    <?php endwhile; ?>

    <?php else: ?>

        <article>

            <h1><?php _e( 'Sorry, nothing to display.', 'paperplane' ); ?></h1>

        </article>

    <?php endif; ?>

    </section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
