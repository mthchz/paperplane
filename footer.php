            <footer class="footer" role="contentinfo">
                <div class="footer--inner">
                    <nav class="nav-footer">
                        <?php paperplane_footer_nav(); ?>
                    </nav>
                </div>

                <p class="copyright">
                    &copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>. <?php _e('Powered by', 'paperplane'); ?>
                            <a href="//wordpress.org" title="WordPress">WordPress</a>
                </p>

            </footer>
            <a href="#top" class="back-to-top js-scroll-to js-gotop" title="Revenir en haut"><span class="assistive-text">Revenir en haut</span></a>

        </div>

        <?php wp_footer(); ?>

        <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo get_wptheme_option('google_analytics', 'code'); ?>"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', '<?php echo get_wptheme_option('google_analytics', 'code'); ?>');
        </script>


        <!--[if lte IE 9]>
        <div class="old_browser">
            <p>Vous utilisez un navigateur web trop ancien pour profiter pleinement de ce site !<br>
            Si vous en avez la possibilité, mettez le à jour (<a target="_blank" href="http://windows.microsoft.com/fr-fr/internet-explorer/download-ie" rel="external nofollow">plus d'info</a>)
            ou choississez un navigateur plus récent <a  target="_blank" href="http://outdatedbrowser.com/" rel="external nofollow">en suivant ce lien.</a></p>
        </div>
        <![endif]-->

    </body>
</html>
