<?php
/*
 *  Author: Mathieu Cheze | @mthchz
 *
 */
define('THEME_PATH', get_stylesheet_directory());
define('WIDGETS_PATH', THEME_PATH . '/wp-features/widgets/');
/* ============================================================================= */
/*  External Modules/Files
/* ============================================================================= */
// require_once (WIDGETS_PATH . 'last-actus.php');

/* ============================================================================= */
/*  Theme customization/features
/* ============================================================================= */
require_once (THEME_PATH . '/wp-features/wp-theme-settings.php'); // Theme settings in admin panel
// require_once (THEME_PATH . '/wp-features/wp-post-types.php'); // Custom post types
// require_once (THEME_PATH . '/wp-features/wp-taxonomies.php'); // Custom taxonomies
// require_once (THEME_PATH . '/wp-features/wp-shortcode.php'); // Custom shortcodes

/* ============================================================================= */
/*   LIBS
/* ============================================================================= */

/* Load scripts
*******************************************************************************/
function paperplane_scripts() {
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        // wp_register_script('conditionizr', get_template_directory_uri() . '/includes/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        // wp_enqueue_script('conditionizr'); // Enqueue it!

        // wp_register_script('modernizr', get_template_directory_uri() . '/includes/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        // wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('app-script', get_template_directory_uri() . '/bundle/js/bundle.js', array('jquery'), '1.0.0', true); // App scripts
        wp_enqueue_script('app-script'); // Enqueue it!
    }
}
add_action('init', 'paperplane_scripts'); // Add Custom Scripts to wp_head

/* Load  styles
*******************************************************************************/
function paperplane_styles() {
    wp_register_style('main-style', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('main-style'); // Enqueue it!
}
add_action('wp_enqueue_scripts', 'paperplane_styles'); // Add Theme Stylesheet

/* Add ie conditional html5 shim to header
*******************************************************************************/
function add_ie_html5_shim () {
    echo '<!--[if lt IE 9]>';
    echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>';
    echo '<![endif]-->';
}
add_action('wp_head', 'add_ie_html5_shim');

/* ============================================================================= */
/*  Theme Support
/* ============================================================================= */
if (function_exists('add_theme_support')) {
    // Add Logo Support
    add_theme_support('custom-logo');

    // Add Menu Support
    add_theme_support('menus');

    // Enables post and comment RSS feed links to head
    //add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('paperplane', get_template_directory() . '/languages');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');

    // Custom image size
    $customs_images_sizes = array(
        'custom_size' => array(
            'w' => 700,
            'h' => 200,
            'crop' => true,
            'title' => 'Miniature personnalisée'
        )
    );

}

/* Add Customs Images sizes
/* TODO : Add srcset https://iamsteve.me/blog/entry/using-srcset-with-the-post-thumbnail-in-wordpress
*******************************************************************************/
if ( isset($customs_images_sizes) && !empty($customs_images_sizes) ) {

    $GLOBALS['customs_images_sizes_title'] = array();

    // Add image size
    foreach($customs_images_sizes as $name => $image_size){
        $_w = isset($image_size['w']) ? $image_size['w'] : '';
        $_h = isset($image_size['h']) ? $image_size['h'] : '';
        $_crop = isset($image_size['crop']) ? $image_size['crop'] : false;
        $_title = isset($image_size['title']) ? $image_size['title'] : $name;

        add_image_size($name, $_w, $_h, $_crop);
        $customs_images_sizes_title[$name] = __( $_title );
    }

    function customize_image_size_label( $sizes ) {
        global $customs_images_sizes_title;
        return array_merge( $sizes, $customs_images_sizes_title );
    }
    add_filter( 'image_size_names_choose', 'customize_image_size_label' );
}

/* ============================================================================= */
/*  FUNCTIONS : Override and customize WP
/* ============================================================================= */

/* paperplane navigation
*******************************************************************************/
/* Main nav */
function paperplane_nav() {
    wp_nav_menu(array(
        'theme_location'  => 'header-menu',
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul class="header-menu menu">%3$s</ul>',
        'depth'           => 0,
        'walker'          => new paperplane_headermenu_walker()
        )
    );
}

/** Walker for header menu **/
class paperplane_headermenu_walker extends Walker_Nav_Menu {
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="header-menu--menu-item '. esc_attr( $class_names ) . '"';

        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="header-menu--menu-item-link"';

        if($depth != 0) {
            $description = $append = $prepend = "";
        }

        $item_output  = $args->before;
        // $item_output .= '<span>'. apply_filters( 'the_title', $item->title, $item->ID ) .'</span>';
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before.apply_filters( 'the_title', $item->title, $item->ID );
        $item_output .= $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

/* Footer nav */
function paperplane_footer_nav() {
    wp_nav_menu(array(
        'theme_location'  => 'footer-menu',
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul class="footer-menu menu">%3$s</ul>',
        'depth'           => 0,
        'walker'          => new paperplane_footermenu_walker()
        )
    );
}

/** Walker for header menu **/
class paperplane_footermenu_walker extends Walker_Nav_Menu {
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="footer-menu--menu-item '. esc_attr( $class_names ) . '"';

        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="footer-menu--menu-item-link"';

        if($depth != 0) {
            $description = $append = $prepend = "";
        }

        $item_output  = $args->before;
        // $item_output .= '<span>'. apply_filters( 'the_title', $item->title, $item->ID ) .'</span>';
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before.apply_filters( 'the_title', $item->title, $item->ID );
        $item_output .= $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

/* Register paperplane Navigation
*******************************************************************************/
function register_main_menu() {
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu'  => __('Header Menu', 'paperplane'), // Main Navigation
        'footer-menu'  => __('Footer Menu', 'paperplane'), // Main Navigation
    ));
}
add_action('init', 'register_main_menu'); // Add paperplane Menu

/* Add page slug to body class, love this - Credit: Starkers Wordpress Theme
*******************************************************************************/
// function add_slug_to_body_class($classes) {
//     global $post;
//     if (is_home()) {
//         $key = array_search('blog', $classes);
//         if ($key > -1) {
//             unset($classes[$key]);
//         }
//     } elseif (is_page()) {
//         $classes[] = sanitize_html_class($post->post_name);
//     } elseif (is_singular()) {
//         $classes[] = sanitize_html_class($post->post_name);
//     }
//     $classes[]="";
//     return $classes;
// }
// add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)


/* If Dynamic Sidebar Exists
*******************************************************************************/
if (function_exists('register_sidebar')) {
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area', 'paperplane'),
        'description' => __('Description for this widget-area', 'paperplane'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

/* SEO proof : remove h1 & color front TinyMCE
*******************************************************************************/
//unset "select color"
function wpc_boutons_tinymce_2($buttons) {
    array_unshift( $buttons, 'styleselect' );
    $buttons[3] = '';
    return $buttons;
}
add_filter("mce_buttons_2", "wpc_boutons_tinymce_2");

// Hide H1 from tinyMCE
function my_mce_hide_h1($arr){
    $arr['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5';
    return $arr;
}
add_filter('tiny_mce_before_init', 'my_mce_hide_h1');

/* Responsive Wrap for oEmbed
*******************************************************************************/
add_filter('embed_oembed_html', 'paperplane_oembed_html', 99, 4);
function paperplane_oembed_html($html, $url, $attr, $post_id) {
    return '<div id="responsive-iframewrap">' . $html . '</div>';
}

/* Custom format TinyMCE
*******************************************************************************/
function my_mce_before_init( $settings ) {

    $style_formats = array(
        array(
            'title'    => __('Download item list', 'paperplane'),
            'selector' => 'ul',
            'classes'  => 'download-doc'
        ),
        array(
            'title'    => __('Download item', 'paperplane'),
            'selector' => 'a',
            'classes'  => 'download-doc-link'
        ),
        array(
            'title'    => __('Link', 'paperplane'),
            'selector' => 'a',
            'classes'  => 'content-link'
        )
    );

    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;

}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init' );

/* Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
*******************************************************************************/
function paperplane_pagination() {
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}
add_action('init', 'paperplane_pagination'); // Add our HTML5 Pagination

/* Customize WP-Navi for foundation
*******************************************************************************/
// function paperplane_pagination($html) {
//     $out = '';
//
//     $out = str_replace("<div","",$html);
//     $out = str_replace("class='wp-pagenavi'>","",$out);
//     $out = str_replace("<a","<li><a",$out);
//     $out = str_replace("</a>","</a></li>",$out);
//     $out = str_replace('<span','<li class="current"><a href="#"',$out);
//     $out = str_replace("</span>","</a></li>",$out);
//     $out = str_replace("</div>","",$out);
//
//     return '<ul class="pagination">'.$out.'</ul>';
// }
// add_filter( 'wp_pagenavi', 'paperplane_pagination', 10, 2 );

/* Create the Custom Excerpts callback
*******************************************************************************/
function paperplane_excerpt($limit = 20) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);

    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }

    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

    return $excerpt;
}

/* Custom View Article link to Post
*******************************************************************************/
function paperplane_view_article($more) {
    global $post;
    return '<a class="view-article" href="' . get_permalink($post->ID) . '">...</a>';
}
add_filter('excerpt_more', 'paperplane_view_article'); // Add 'View Article' button instead of [...] for Excerpts

/* Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
*******************************************************************************/
function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

/* Custom Gravatar in Settings > Discussion
*******************************************************************************/
// function paperplane_gravatar ($avatar_defaults) {
//     $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
//     $avatar_defaults[$myavatar] = "Custom Gravatar";
//     return $avatar_defaults;
// }
// add_filter('avatar_defaults', 'paperplane_gravatar'); // Custom Gravatar in Settings > Discussion
//
/* Threaded Comments
*******************************************************************************/
// function enable_threaded_comments() {
//     if (!is_admin()) {
//         if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
//             wp_enqueue_script('comment-reply');
//         }
//     }
// }
// add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments

/* Add custom class to post class
*******************************************************************************/
function add_content_class($classes) {
    if(is_page() || is_single())
        $classes[] = 'content';

    return $classes;
}
add_filter('post_class', 'add_content_class');

/* Override default post gallery
*******************************************************************************/
// function paperplane_gallery($output, $attr) {
    //global $post;

    /*$gallery = get_posts( array(
        'include'   => $attr['ids'],
        'post_type' => 'attachment',
        'orderby'   => 'post_date',
        'order'     => 'ASC'
    ) );

    $output .= '<div class="post-gallery row collapse">';
    $custom_galId = str_replace(',', '', $attr['ids']);

    foreach($gallery as $image){
        $output .= '<a class="post-gallery__item small-6 medium-4 large-2 inline-block" data-lightbox-gallery="lightbox['.$custom_galId.']" rel="lightbox['.$custom_galId.']" href="'. wp_get_attachment_url( $image->ID ).'">';
        $output .= wp_get_attachment_image($image->ID, 'medium-thumb');
        $output .= '<p class="post-gallery__item--legend text-center">'.$image->post_excerpt.'</p>';
        $output .= '</a>';
    }

    $output .= '</div>';

    return $output;*/
// }
// add_filter('post_gallery', 'paperplane_gallery', 11, 2);

/* ============================================================================= */
/*   Breadcrumbs
/* ============================================================================= */
function wp_breadcrumb(){
    // Settings
    $separator          = ' / ';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Accueil';

    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = '';

    // Get the query & post information
    global $post,$wp_query;

    $html = '';

    // Do not display on the homepage
    if ( !is_front_page() ) {
        // Build the breadcrums
        $html .= '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
        // Home page
        $html .= '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        $html .= '<li class="separator separator-home"> ' . $separator . ' </li>';

        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
            $html .= '<li class="item-current item-archive"><span class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</span></li>';
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
            // If post is a custom post type
            $post_type = get_post_type();
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
                $html .= '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                $html .= '<li class="separator"> ' . $separator . ' </li>';
            }
            $custom_tax_name = get_queried_object()->name;
            $html .= '<li class="item-current item-archive"><span class="bread-current bread-archive">' . $custom_tax_name . '</span></li>';
        } else if ( is_single() ) {
            // If post is a custom post type
            $post_type = get_post_type();
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
                $html .= '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                $html .= '<li class="separator"> ' . $separator . ' </li>';
            }

            // Get post category info
            $category = get_the_category();
            if(!empty($category)) {
                // Get last category post is in
                $last_category = end(array_values($category));
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';

                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
            }

            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
            }

            // Check if the post is in a category
            if(!empty($last_category)) {
                $html .= $cat_display;
                $html .= '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                $html .= '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                $html .= '<li class="separator"> ' . $separator . ' </li>';
                $html .= '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
            } else {
                $html .= '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
            }
        } else if ( is_category() ) {
            // Category page
            $html .= '<li class="item-current item-cat"><span class="bread-current bread-cat">' . single_cat_title('', false) . '</span></li>';
        } else if ( is_page() ) {
            // Standard page
            if( $post->post_parent ){
                // If child page, get parents
                $anc = get_post_ancestors( $post->ID );
                // Get parents in the right order
                $anc = array_reverse($anc);

                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                // Display parent pages
                $html .= $parents;
                // Current page
                $html .= '<li class="item-current item-' . $post->ID . '"><span title="' . get_the_title() . '"> ' . get_the_title() . '</span></li>';
            } else {
                // Just display current page if not parents
                $html .= '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</span></li>';
            }
        } else if ( is_tag() ) {
            // Tag page
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
            // Display the tag name
            $html .= '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><span class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</span></li>';
        } elseif ( is_day() ) {
            // Day archive
            // Year link
            $html .= '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            $html .= '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
            // Month link
            $html .= '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            $html .= '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
            // Day display
            $html .= '<li class="item-current item-' . get_the_time('j') . '"><span class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</span></li>';
        } else if ( is_month() ) {
            // Month Archive
            // Year link
            $html .= '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            $html .= '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
            // Month display
            $html .= '<li class="item-month item-month-' . get_the_time('m') . '"><span class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</span></li>';
        } else if ( is_year() ) {
            // Display year archive
            $html .= '<li class="item-current item-current-' . get_the_time('Y') . '"><span class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</span></li>';
        } else if ( is_author() ) {
            // Auhor archive
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );

            // Display author name
            $html .= '<li class="item-current item-current-' . $userdata->user_nicename . '"><span class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</span></li>';
        } else if ( get_query_var('paged') ) {
            // Paginated archives
            $html .= '<li class="item-current item-current-' . get_query_var('paged') . '"><span class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</span></li>';
        } else if ( is_search() ) {
            // Search results page
            $html .= '<li class="item-current item-current-' . get_search_query() . '"><span class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</span></li>';
        } elseif ( is_404() ) {
            // 404 page
            $html .= '<li>' . 'Error 404' . '</li>';
        }
        $html .= '</ul>';
    }

    return $html;
}

/* ============================================================================= */
/*   Post sharer
/* ============================================================================= */
function paperplane_postsharer(){
    $title = get_the_title();
    $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    $html = '<div class="share-button">
        <div class="share-button--button">
            <a class="share-button--icon share-button--facebook" target="_blank" title="Partager sur Facebook" href="https://www.facebook.com/sharer.php?u='.$url.'" rel="nofollow" onclick="javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=700\');return false; ga(\'send\', \'event\', \'Partage\', \'facebook\', \''.$title.'\');">
                <span class="icon-facebook"></span>
            </a>
            <a class="share-button--icon share-button--twitter" target="_blank" title="Partager sur Twitter" href="https://twitter.com/share?url='.$url.'&amp;text='.$title.'" rel="nofollow" onclick="javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=700\');return false; ga(\'send\', \'event\', \'Partage\', \'twitter\', \''.$title.'\');">
                <span class="icon-twitter"></span>
            </a>
            <a class="share-button--icon share-button--google" target="_blank" title="Partager sur Google +" href="https://plus.google.com/share?url='.$url.'&amp;text='.$title.'" rel="nofollow" onclick="javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=700\');return false; ga(\'send\', \'event\', \'Partage\', \'google\', \''.$title.'\');">
                <span class="icon-google-plus"></span>
            </a>
            <a class="share-button--icon share-button--linkedin" target="_blank" title="Partager sur LinkedIn" href="https://www.linkedin.com/shareArticle?mini=true&url='.$url.'&amp;title='.$title.'" rel="nofollow" onclick="javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=700\');return false; ga(\'send\', \'event\', \'Partage\', \'google\', \''.$title.'\');">
                <span class="icon-linkedin2"></span>
            </a>
        </div>
    </div>';

    return $html;
}
