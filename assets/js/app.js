/**
 * App.js
 */

// Includes libs (use with gulp-include)
//= require ../includes/foundation-sites/dist/js/foundation.min.js

// Global script var
var currentMediaQuery;

// Main
(function($) {
    'use strict';

    // Firing foundation
    $(document).foundation();
    currentMediaQuery = Foundation.MediaQuery.current;

    // Elements
    var $btnMenu = $('.js-menu-button');
    var $menu = $('.js-menu');
    var $goTopButton = $('.js-gotop');

    /* SCROLLTO ANCHOR
    *******************************************************************************/
    $('.js-scroll-to').on('click', function(event) {
        event.preventDefault();

        var link = $(this).attr('href');
        var hashExpression = new RegExp("#([a-zA-Z0-9\-\_]*)$");

        if( hashExpression.test(link) ){

            var target = this.hash,
                $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 400, 'swing');

            return;
        }
    });

    /* Show/hide "go top" button
     *******************************************************************************/
    $(document).on('scroll DOMNodeInserted', function() {
        var scroll = $(window).scrollTop();

        if( scroll >= $(window).height() ){ // Show
            $goTopButton.addClass('is-active');
        }
        if( scroll <= $(window).height() ){ // Hide
            $goTopButton.removeClass('is-active');
        }

    });

    /* Responsive menu
     *******************************************************************************/
    $(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
        if( Foundation.MediaQuery.atLeast('large') ){
            $btnMenu.hide();
            $menu.show();
        } else {
            $menu.hide();
            $btnMenu.show();
        }
        currentMediaQuery = Foundation.MediaQuery.current;
    });

    if( currentMediaQuery === 'medium' || currentMediaQuery === 'small' ){
        $menu.hide();
        $btnMenu.show();
        $btnMenu.removeClass('is-open');
    } else {
        $menu.show();
        $btnMenu.hide();
        $btnMenu.removeClass('is-open');
    }

    $btnMenu.on('click', function(e) {
        e.preventDefault();

        if( $btnMenu.hasClass('is-open') ){
            $menu.hide('400');
            $btnMenu.removeClass('is-open');
        } else {
            $menu.show('400');
            $btnMenu.addClass('is-open');
            $('html, body').stop().animate({
                'scrollTop': $btnMenu.offset().top
            }, 400, 'swing');

        }
    });

    /* Element is visible ?
    *******************************************************************************/
    // $.fn.isVisible = function(partial) {
    //
    //     if( typeof $(this).offset() === 'undefined' ) return false;
    //
    //     var $t = $(this),
    //         $w = $(window),
    //         viewTop = $w.scrollTop(),
    //         viewBottom = viewTop + $w.height() - 100,
    //         _top = $t.offset().top,
    //         _bottom = _top + $t.height(),
    //         compareTop = partial === true ? _bottom : _top,
    //         compareBottom = partial === true ? _top : _bottom;
    //
    //     return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    // };

    /* OuterHtml
    *******************************************************************************/
    // if( !$.outerHTML ){
    //     $.extend({
    //         outerHTML: function(ele) {
    //             if( ele.length === 1 ) return ele[0].outerHTML;
    //             var $return = [];
    //             ele.each(function(i) {
    //                 $return.push($(this)[0].outerHTML);
    //             });
    //             return $return;
    //         }
    //     });
    //     $.fn.extend({
    //         outerHTML: function() {
    //             return $.outerHTML($(this));
    //         }
    //     });
    // }

})(jQuery);