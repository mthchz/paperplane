#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Paperplane\n"
"POT-Creation-Date: 2015-12-15 17:34+0100\n"
"PO-Revision-Date: 2015-12-15 17:34+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.5\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: 404.php:6
msgid "Page not found"
msgstr ""

#: 404.php:8
msgid "Return home?"
msgstr ""

#: archive.php:4
msgid "Archives"
msgstr ""

#: author.php:7
msgid "Author Archives for "
msgstr ""

#: author.php:13
msgid "About "
msgstr ""

#: author.php:34 loop-search.php:22 loop.php:22 single.php:21
msgid "Published by"
msgstr ""

#: author.php:35 loop-search.php:23 loop.php:23 single.php:22
msgid "Leave your thoughts"
msgstr ""

#: author.php:35 loop-search.php:23 loop.php:23 single.php:22
msgid "1 Comment"
msgstr ""

#: author.php:35 loop-search.php:23 loop.php:23 single.php:22
msgid "% Comments"
msgstr ""

#: author.php:51 loop-search.php:38 loop.php:37 page.php:26 single.php:44
msgid "Sorry, nothing to display."
msgstr ""

#: category.php:5
msgid "Categories for "
msgstr ""

#: comments.php:3
msgid "Post is password protected. Enter the password to view any comments."
msgstr ""

#: comments.php:18
msgid "Comments are closed here."
msgstr ""

#: footer.php:11
msgid "Powered by"
msgstr ""

#: functions.php:241
msgid "Header Menu"
msgstr ""

#: functions.php:242
msgid "Footer Menu"
msgstr ""

#: functions.php:272
msgid "Widget Area"
msgstr ""

#: functions.php:273
msgid "Description for this widget-area"
msgstr ""

#: functions.php:310
msgid "Download item list"
msgstr ""

#: functions.php:315
msgid "Download item"
msgstr ""

#: functions.php:320
msgid "Link"
msgstr ""

#: functions.php:426 functions.php:428
msgid "Search on website"
msgstr ""

#: functions.php:429
msgid "Go !"
msgstr ""

#: includes/wp.theme-settings.php:27
msgid "Theme Options"
msgstr ""

#: includes/wp.theme-settings.php:28
msgid "Global theme settings"
msgstr ""

#: includes/wp.theme-settings.php:43
msgid "Disable"
msgstr ""

#: includes/wp.theme-settings.php:44
msgid "Enable"
msgstr ""

#: index.php:5
msgid "Latest Posts"
msgstr ""

#: search.php:5
#, php-format
msgid "%s Search Results for "
msgstr ""

#: searchform.php:3
msgid "To search, type and hit enter."
msgstr ""

#: searchform.php:4
msgid "Search"
msgstr ""

#: single.php:26
msgid "Tags: "
msgstr ""

#: single.php:28
msgid "Categorised in: "
msgstr ""

#: single.php:30
msgid "This post was written by "
msgstr ""

#: tag.php:5
msgid "Tag Archive: "
msgstr ""

#: widgets/last-actus.php:16
msgid "Dernières actualités"
msgstr ""

#. Theme Name of the plugin/theme
msgid "Paperplane"
msgstr ""

#. Description of the plugin/theme
msgid "WordPress Theme"
msgstr ""

#. Author of the plugin/theme
msgid "Mathieu Cheze (Proximit Agency)"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.proximit-agency.fr/"
msgstr ""
