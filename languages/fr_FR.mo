��    %      D  5   l      @  
   A     L  	   c     m     t     }     �     �     �     �      �                    .     5     A     W     \     h     u     �     �  D   �  
   �     �     �                     ;     I     P     ^     x     �  �  �     .      =     ^  	   l     v          �     �     �     �     �     	     	     .	     H	     P	  "   d	     �	     �	     �	     �	     �	     �	  j   �	     L
     Z
     f
  
   �
     �
  #   �
     �
     �
     �
       7         X                  "                                   #                                    $   %                                	              
                              !           % Comments %s Search Results for  1 Comment About  Archives Author Archives for  Categories for  Categorised in:  Comments are closed here. Dernières actualités Description for this widget-area Disable Download item Download item list Enable Footer Menu Global theme settings Go ! Header Menu Latest Posts Leave your thoughts Link Page not found Post is password protected. Enter the password to view any comments. Powered by Published by Return home? Search Search on website Sorry, nothing to display. Tag Archive:  Tags:  Theme Options This post was written by  To search, type and hit enter. Widget Area Plural-Forms: nplurals=2; plural=(n > 1);
Project-Id-Version: Paperplane
POT-Creation-Date: 2015-12-15 17:34+0100
PO-Revision-Date: 2015-12-15 17:37+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
X-Poedit-Basepath: ..
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: fr_FR
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 % commentaires %s résultats de recherche pour  1 Commentaire À propos Archives Archives de l’auteur pour Catégories pour Classés dans : Les commentaires sont fermés. Dernières actualités Description pour ce widget … Désactiver Téléchargement Liste de téléchargement Activer Menu dans le footer Les paramètres globaux du thème. Go ! Menu d’entête Articles récents Laissez votre commentaire Lien Page non trouvée L’article est protégé par un mot de passe. Veuillez entrez le mot de passe pour voir les commentaires. Propulsé par Publié par Retour à la page d’accueil ? Rechercher Recherche sur le site Désolé, aucun contenu disponible. Archives par mot-clé : Mots-clé : Options du thème Cet article a été écrit par Recherche, tapez quelque chose et faites « Entrer » Zone de widget 