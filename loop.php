<?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>
            </a>
        <?php endif; ?>

        <h2>
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
        </h2>

        <span class="date"><?php the_date(); ?> <?php the_time(); ?></span>
        <span class="author"><?php _e( 'Published by', 'paperplane' ); ?> <?php the_author_posts_link(); ?></span>
        <span class="comments"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'paperplane' ), __( '1 Comment', 'paperplane' ), __( '% Comments', 'paperplane' )); ?></span>

        <?php paperplane_excerpt('paperplane_index'); // Build your custom callback length in functions.php ?>

    </article>

<?php endwhile; ?>

<?php else: ?>

    <article>
        <h2><?php _e( 'Sorry, nothing to display.', 'paperplane' ); ?></h2>
    </article>

<?php endif; ?>
