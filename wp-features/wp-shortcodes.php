<?php
/*
 *  Shortcodes
 *
 *  Author: Mathieu Cheze
 */

/* ============================================================================= */
/*  ShortCode Functions
/* ============================================================================= */
// EXEMPLE : [bartag foo="foo-value"]
// function bartag_func( $atts ) {
//     $a = shortcode_atts( array(
//         'foo' => 'something',
//         'bar' => 'something else',
//     ), $atts );
//
//     return "foo = {$a['foo']}";
// }
// add_shortcode( 'bartag', 'bartag_func' );
