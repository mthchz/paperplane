<?php
/**
 *    Remonter des dernières actualités (type post)
 *
 *    @class : last_actus_widget extends WP_Widget
 *
 *    @return : html
 *
 *    @author : mthchz
 **/
class last_actus_widget extends WP_Widget
{
    public function __construct() {
        $widget_ops = array('description' => 'Affiche les dernières actus');
        parent::WP_Widget('last_actus_widget', __('Dernières actualités'), $widget_ops);
    }

    public function widget($args, $instance) {
        global $post;
        $post_old = $post; // Save the post object.

        extract($args);
        $title = $instance['title'];
        $classes = $instance['class'];
        $how_many = $instance['how_many'];
        ?>
        <section class="widget-last-actus <?php echo $classes ?>">

            <h2 class="widget-title"><?php echo $title ?></h2>

                <?php
                $args = array(
                    'posts_per_page' => $how_many,
                    'orderby' => 'date',
                    'post_status' => 'publish',
                );

                $postActu = new WP_Query($args);
                if ($postActu->have_posts()): ?>

                    <?php while ($postActu->have_posts()):
                        $postActu->the_post() ?>
                        <div class="widget-last-actus--item">

                            <h3>
                                <a href="<?php the_permalink() ?>" class="widget-last-actus--title"><?php the_title() ?></a>
                            </h3>

                            <?php $category = get_the_category() ?>

                            <a class="widget-last-actus--cat " href="<?php echo esc_url(get_category_link($category[0]->term_id)) ?>"><?php echo $category[0]->cat_name ?></a>
                            <span class="widget-last-actus--date"><?php echo get_the_date() ?></span>

                            <div class="widget-last-actus--desc"><?php paperplane_excerpt(80) ?></div>

                            <a href="<?php the_permalink() ?>" class="widget-last-actus--readmore button">Lire la suite</a>
                        </div>
                    <?php endwhile ?>
            </div>
            <?php endif ?>
            <?php wp_reset_query() ?>

        </section>

        <?php
            $post = $post_old; // Restore the post object.
             wp_reset_query();
    } //end:widget()

    public function update($new_instance, $old_instance) {
        return $new_instance;
    }

    public function form($instance) {
        $title = esc_attr($instance['title']);
        $how_many = $instance['how_many'];
        ?>
        <p>
              <label for="<?php echo $this->get_field_id('title'); ?>">Titre :</label>
              <input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
        </p>
        <p>
              <label for="<?php echo $this->get_field_id('how_many'); ?>">Combien :</label>
              <input type="text" name="<?php echo $this->get_field_name('how_many'); ?>"  value="<?php echo $how_many; ?>" class="widefat" id="<?php echo $this->get_field_id('how_many'); ?>" />
        </p>

          <?php

    } //end:form
}

register_widget('last_actus_widget');
?>
