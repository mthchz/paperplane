<?php
/*
 *  Theme options
 *
 *  Author: Mathieu Cheze
 */

/* ====================================================================
	Init & Set default option
    ================================================================== */
$wptheme_options = get_option( 'wptheme_options' );

$default_wptheme_options['commentaires']['enabled'] = '0';
$default_wptheme_options['test']['test'] = '0';
$default_wptheme_options['google_analytics']['code'] = 'UA-XXXXXXXX-XX';

// Ajout des valeurs par default pour éviter les erreurs
if($wptheme_options === false){
    add_option('wptheme_options', $default_wptheme_options);
}

/* ====================================================================
	Add theme option to admin panel
    ================================================================== */
function wptheme_option_customize_register($wp_customize){
    $wp_customize->add_section('wptheme_options', array(
        'title'          => __('Theme Options', 'paperplane'),
        'description'    => __('Global theme settings', 'paperplane'),
        'priority' => 0,
    ));

    // Add Enable/disable comment
    $wp_customize->add_setting('wptheme_options[commentaires][enabled]', array(
        'default' => $default_wptheme_options['commentaires']['enabled'],
        'capability' => 'edit_theme_options',
        'type' => 'option',
    ));
    $wp_customize->add_control('wptheme_options[commentaires][enabled]', array(
        'label' => 'Comments',
        'section' => 'wptheme_options',
        'type'     => 'radio',
		'choices'  => array(
			'0'  => esc_attr__( 'Disable', 'paperplane' ),
			'1' => esc_attr__( 'Enable', 'paperplane' )
		),
    ));

    // Add Google Analytics section
    $wp_customize->add_setting('wptheme_options[google_analytics][code]', array(
        'default' => $default_wptheme_options['google_analytics']['code'],
        'capability' => 'edit_theme_options',
        'type' => 'option',
    ));

    $wp_customize->add_control('wptheme_options[google_analytics][code]', array(
        'label' => 'Google Analytics ID',
        'section' => 'wptheme_options',
        'type' => 'text',
    ));

}
add_action('customize_register', 'wptheme_option_customize_register');

/* ====================================================================
   Function to get theme option into front end
   ================================================================== */
function get_wptheme_option($option, $name, $default = false) {
    global $wptheme_options;

    $input = ( $wptheme_options[$option] ) ? $wptheme_options[$option] : null;
    return ( isset($input[$name]) ) ? $input[$name] : false ;
}

/* ====================================================================
   DISABLE COMMENT
   ================================================================== */
if(get_wptheme_option('commentaires', 'enabled') === '0'){
    // Disable support for comments and trackbacks in post types
    function df_disable_comments_post_types_support() {
        $post_types = get_post_types();
        foreach ($post_types as $post_type) {
            if(post_type_supports($post_type, 'comments')) {
                remove_post_type_support($post_type, 'comments');
                remove_post_type_support($post_type, 'trackbacks');
            }
        }
    }
    add_action('admin_init', 'df_disable_comments_post_types_support');

    // Close comments on the front-end
    function df_disable_comments_status() {
        return false;
    }
    add_filter('comments_open', 'df_disable_comments_status', 20, 2);
    add_filter('pings_open', 'df_disable_comments_status', 20, 2);

    // Hide existing comments
    function df_disable_comments_hide_existing_comments($comments) {
        $comments = array();
        return $comments;
    }
    add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);

    // Remove comments page in menu
    function df_disable_comments_admin_menu() {
        remove_menu_page('edit-comments.php');
    }
    add_action('admin_menu', 'df_disable_comments_admin_menu');

    // Redirect any user trying to access comments page
    function df_disable_comments_admin_menu_redirect() {
        global $pagenow;
        if ($pagenow === 'edit-comments.php') {
            wp_redirect(admin_url()); exit;
        }
    }
    add_action('admin_init', 'df_disable_comments_admin_menu_redirect');

    // Remove comments metabox from dashboard
    function df_disable_comments_dashboard() {
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
    }
    add_action('admin_init', 'df_disable_comments_dashboard');

    // Remove comments links from admin bar
    function df_disable_comments_admin_bar() {
        if (is_admin_bar_showing()) {
            remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
        }
    }
    add_action('init', 'df_disable_comments_admin_bar');
}

?>
