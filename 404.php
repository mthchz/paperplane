<?php get_header(); ?>

    <section class="main" role="main">
        <article id="post-404">

            <h1><?php _e( 'Page not found', 'paperplane' ); ?></h1>

            <p>Désolé, la page que vous recherchez n'existe pas.</p>
            <p>Nous vous conseillons de <a href="<?php echo home_url(); ?>">revenir à la page d'accueil</a> ou d'utiliser le menu principal en en-tête</p>
            <p>Si vous ne pouvez pas trouver ce que vous cherchez sur notre site, ou si vous avez une question n'hésitez pas à nous contacter</p>
            <hr>

        </article>

    </section>


<?php get_sidebar(); ?>

<?php get_footer(); ?>
