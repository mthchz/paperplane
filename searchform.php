<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
    <div class="input-group">

        <input class="search-input input-group-field" type="search" name="s" placeholder="<?php _e( 'To search, type and hit enter.', 'paperplane' ); ?>">

        <div class="input-group-button">
            <button class="search-submit button" type="submit" role="button"><?php _e( 'Search', 'paperplane' ); ?></button>
        </div>
    </div>
</form>
