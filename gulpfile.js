/* ============================================================================= */
/*  Commandes
/* ============================================================================= */
// Pour créer un simple build
// $ gulp build

// Pour créer un build de production
// $ gulp production

// Pour surveiller les changements de scss et compiler
// $ gulp watch

// Pour surveiller les changements de scss, compiler et rafraichir avec browsrSync
// $ gulp watch --sync=projet.dev

/* ============================================================================= */
/*  Variables
/* ============================================================================= */
// Requis
var gulp = require('gulp');
var del = require('del');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync').create();
var Promise = require('es6-promise').Promise;
var plugins = require('gulp-load-plugins')(); // tous les plugins de package.json
var log = require('fancy-log');
var critical = require('critical');

// Var
var isProduction = false;

// Project paths
var SITE_HOME = '';
var paths = {
    scss: [
        //'assets/includes/foundation-sites/assets/',
        'assets/includes/foundation-sites/scss/',
        'assets/scss'
    ],
    watch_scss: [
        //'assets/includes/foundation-sites/assets/**/*.scss',
        'assets/includes/foundation-sites/scss/**/*.scss',
        'assets/scss/*.scss'
    ],
    js:[
        'assets/js/*.js'
    ],
    images:[
        'assets/images/*'
    ]
};

/* ============================================================================= */
/*  Bundle tasks
/* ============================================================================= */

/* Clean bundle
*******************************************************************************/
gulp.task('clean', function() {
    return del(['bundle/**/*']);
});

/* Complie CSS
*******************************************************************************/
gulp.task('compile-css', function() {
    return gulp.src('assets/scss/*')
        .pipe(plugins.flatten())
        .pipe(plugins.sourcemaps.init({loadMaps:true}))
        .pipe(plugins.compass({
            config_file: 'config.rb',
            import_path: [
                //'assets/includes/foundation-sites/assets/',
                'assets/includes/foundation-sites/scss/'
            ],
            force: false,
            css: '',
            sass: 'assets/scss',
            style: (isProduction ? 'compressed' : 'nested'),
            sourcemap: (isProduction ? false : true)
        }))
        .pipe(plugins.if(!isProduction, plugins.sourcemaps.init({loadMaps: true})))
        .pipe(plugins.autoprefixer({
            browsers: ["last 2 versions", "> 1%", "Explorer 9", "Android 4", "iOS 7"],
            flexbox: true
        }))
        .pipe(plugins.if(isProduction, plugins.csso({
            restructure: true,
            sourceMap: false,
            debug: false
        })))
        .pipe(plugins.if(!isProduction, plugins.sourcemaps.write('')))
        .pipe(gulp.dest(''));
});

/* Generate critical CSS
*******************************************************************************/
gulp.task('critical-css', function() {
    if(SITE_HOME === ''){
        log('The parameter SITE_HOME is required. Please provide this into Project Path inside gulpfile.js');
        log('WARNING Critical CSS not generate : SITE_HOME empty.');
        return;
    }

    return critical.generate({
        src: SITE_HOME,
        base: './',
        dest: 'bundle/css/critical.css',
        minify: true
    });
});

/* Optimize images
*******************************************************************************/
gulp.task('optimize-images', function() {
    return gulp.src(paths.images)
        .pipe(plugins.imagemin())
        .pipe(gulp.dest('bundle/images'));
});

/* Bundle Scripts
*******************************************************************************/
gulp.task('bundle-js', function() {
    return gulp.src('assets/js/*')
        .pipe(plugins.include({
            extensions: "js",
            hardFail: true,
            includePaths: [
                __dirname + "/assets/js/includes",
                __dirname + "/assets/js"
            ]
        })).on('error', console.log)
        .pipe(plugins.concat('bundle.js'))
        .pipe(plugins.if(isProduction, plugins.minify({
            ext:{
                min:'.js'
            },
            noSource: true
        })))
        .pipe(gulp.dest('bundle/js'));
});

/* Browser sync
*******************************************************************************/
gulp.task('browser-sync', function() {
    if(SITE_HOME === ''){
        log('The parameter SITE_HOME is required. Please provide this into Project Path inside gulpfile.js');
        log('WARNING Browsersync not launched : SITE_HOME empty.');
        return;
    }

    // watch files
    var files = [
        '*.css',
        'bundle/**/*',
        '*.php'
    ];

    //initialize browsersync
    browserSync.init(files, {
        localOnly: true,
        proxy: SITE_HOME, // browsersync with a php server
        notify: false
    });
});

/* ============================================================================= */
/*  Main tasks
/* ============================================================================= */

/* Build task
*******************************************************************************/
gulp.task('build', ['compile-css', 'optimize-images', 'bundle-js'], function() {
    return;
});

/* Production task
*******************************************************************************/
gulp.task('production', function() {
    isProduction = true;
    runSequence('clean', ['compile-css', 'optimize-images', 'bundle-js'], 'critical-css'); // Run clean first
    return;
});

/* Watch task
*******************************************************************************/
gulp.task('watch', function(){
    // Run browser sync
    if(SITE_HOME !== ''){
        runSequence('browser-sync');
    }

    // Watch Sass
    gulp.watch(paths.watch_scss, ['compile-css']);
    gulp.watch(paths.images, ['optimize-images']);
    gulp.watch(paths.js, ['bundle-js']);
});

/* Default task
*******************************************************************************/
gulp.task('default', function() {
    console.log('Hello World!');
});