# Paperplane
Wordpress Foundation 6 starter theme

## Features
- Foundation 6
- SASS
- Gulp
    - compass
    - autoprefixer
    - csso
    - watch
    - imagemin
    - minify
    - concat-js
    - browser-sync
    - critical-css
- Theme basic pour Wordpress

## Avec Gulp

### Qu'est ce qu'il se passe ?
Tous les fichiers de themes contenus dans _assets_ sont optimisés et déplacés dans le _bundle_, selon la demande pour le developpement ou la production.

#### Styles
En mode build simple : compile les fichiers _.scss_ en _.css_ (à la racine du thème), autoprefixe les propriétés CSS, crée un fichier _style.map.css_.

En mode production : build + suppression des sources map, optimisation CSSO et minification des styles + generation automatique du Critical CSS.

#### Images
Build et production : Les images sont compressées (png, jpg, gif et svg). [Plus d'info...](https://www.npmjs.com/package/gulp-imagemin)

#### Scripts
En mode build simple : concaténation de tous les fichiers JS contenus dans _assets/js_ dans le fichier _bundle.js_.

En mode production : build + minification.

### Installation
`$ cd /path/to/wp/wp-content/themes/paperplane`

`$ npm install` (_installation des paquets basés sur le package.json_)

Renseigner la variable SITE\_HOME avec l'URL de dev du site dans le gulpfile.js

### Commandes
- Simple build des styles, scripts et images : `$ gulp build`
- Build pour la production : `$ gulp production`
- Pour surveiller les changements : `$ gulp watch`
- Pour surveiller les changements, et rafraichir la page automatiquement la variable SITE\_HOME doit être renseignée.

Chaque tâches peut être lancé séparement des processus :
- `$ gulp clean` : nettoie le repertoire _bundle_
- `$ gulp compile-css` : compile les styles, en mode simple
- `$ gulp optimize-images` : optimise les images
- `$ gulp bundle-js` : concatène les JS
- `$ gulp critical-css` : génère le critical CSS automatiquement (la variable SITE\_HOME doit être renseignée)
- `$ gulp browser-sync` : lance le rafraichissement automatique (la variable SITE\_HOME doit être renseignée)

### Important
Pour le theming, editez seulement les fichiers _assets_. Les fichiers dans le repertoire _bundle_ ne doivent pas être directement modifier, ce repertoire est "nettoyé" lors des builds de production.

### What's next ?
- Ajout de test unitaires
- Gutenberg ready
- Réorganisation des fichiers de fonctionnalités de WP (_functions.php_, include des Custom Post Type, etc.)
