<?php get_header(); ?>

    <section class="main" role="main">

        <h1><?php _e( 'Latest Posts', 'paperplane' ); ?></h1>

        <?php get_template_part('loop'); ?>

        <?php get_template_part('pagination'); ?>

    </section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
